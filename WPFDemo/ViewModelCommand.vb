﻿Imports System.ComponentModel

Namespace MVVM
    ' ボタンなどのイベント用の汎用コマンドクラス
    Public Class ViewModelCommand
        Implements ICommand

        Private _executeAction As Action(Of Object)

        Public Sub New(ByVal action As Action(Of Object))
            Me._executeAction = action
        End Sub

        Public Function CanExecute(parameter As Object) As Boolean Implements ICommand.CanExecute
            Return True
        End Function

        Public Event CanExecuteChanged(sender As Object, e As EventArgs) Implements ICommand.CanExecuteChanged

        Public Sub Execute(parameter As Object) Implements ICommand.Execute
            Me._executeAction(parameter)
        End Sub
    End Class
End Namespace
