﻿Imports System.ComponentModel
Imports System.Collections.ObjectModel

Namespace MVVM
    Public Class ViewModel
        Implements INotifyPropertyChanged

        ' ### INotifyPropertyChanged ###
        Public Event PropertyChanged(sender As Object, e As PropertyChangedEventArgs) Implements INotifyPropertyChanged.PropertyChanged

        Private Sub NotifyPropertyChanged(ByVal info As String)
            RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(info))
        End Sub

        ' for count
        Private _count As UInt32 = 0

        ' for Label
        Private _text4Label As String
        Public Property Text4Label() As String
            Get
                Return Me._text4Label
            End Get
            Set(value As String)
                If value <> Me._text4Label Then
                    Me._text4Label = value
                    NotifyPropertyChanged("Text4Label")
                End If
            End Set
        End Property

        ' for DataGrid
        Private _items As ObservableCollection(Of Item)
        Public ReadOnly Property Items() As ObservableCollection(Of Item)
            Get
                Return Me._items
            End Get
        End Property

        ' New
        Public Sub New()
            Me.Text4Label = "DateTime : " + DateTime.Now
            Me._items = New ObservableCollection(Of Item)
        End Sub

        ' command for Add Button
        Public Property CommandBtnAdd As ICommand = New ViewModelCommand(AddressOf BtnAddAction)
        Private Sub BtnAddAction(parameter As Object)
            Me._count += 1
            Me.Text4Label = String.Format("DateTime : {0}", DateTime.Now)
            Me._items.Add(New Item() With { _
                          .ID = String.Format("ID-{0}", _count), _
                          .Name = String.Format("Name-{0}", _count), _
                          .Age = _count _
                      })
        End Sub


        ' command for Clear Button
        Public Property CommandBtnClear As ICommand = New ViewModelCommand(AddressOf BtnClearAction)
        Private Sub BtnClearAction(parameter As Object)
            Me._count = 0
            Me.Text4Label = String.Format("DateTime : {0}", DateTime.Now)
            Me._items.Clear()
        End Sub
    End Class
End Namespace
