﻿Imports System.ComponentModel
Imports System.Collections.ObjectModel

' 地域コード
Public Enum RegionType As Integer
    REGION01 ' 北海道
    REGION02 ' 東北
    REGION03 ' 関東
    REGION04 ' 中部
    REGION05 ' 近畿
    REGION06 ' 中国
    REGION07 ' 四国
    REGION08 ' 九州
    REGION09 ' 沖縄
End Enum

' 地域
Public Class Region
    Public Property Code As RegionType
    Public Property Name As String
End Class

' 都道府県
Public Class Prefecture
    Public Property Code As String
    Public Property Region As RegionType
    Public Property Name As String
End Class

Public Class ViewModel
    Implements INotifyPropertyChanged

    ' ### INotifyPropertyChanged ###
    Public Event PropertyChanged(sender As Object, e As PropertyChangedEventArgs) Implements INotifyPropertyChanged.PropertyChanged

    Private Sub NotifyPropertyChanged(ByVal info As String)
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(info))
    End Sub

    ' 地域
    Private _regions As ObservableCollection(Of Region)
    Public ReadOnly Property Regions() As ObservableCollection(Of Region)
        Get
            Return Me._regions
        End Get
    End Property

    ' 都道府県
    Private _prefectures As ObservableCollection(Of Prefecture)
    Public ReadOnly Property Prefectures As IEnumerable(Of Prefecture)
        Get
            Return Me._prefectures.Where(Function(item) item.Region = _selectedRegion)
        End Get
    End Property

    ' 選択されている地域
    Private _selectedRegion As RegionType
    Public Property SelectedRegion() As RegionType
        Get
            Return _selectedRegion
        End Get
        Set(value As RegionType)
            _selectedRegion = value
            NotifyPropertyChanged("Prefectures")
        End Set
    End Property

    ' New
    Public Sub New()
        ' 地域
        Me._regions = New ObservableCollection(Of Region) From { _
            New Region With {.Code = RegionType.REGION01, .Name = "北海道"},
            New Region With {.Code = RegionType.REGION02, .Name = "東北"},
            New Region With {.Code = RegionType.REGION03, .Name = "関東"},
            New Region With {.Code = RegionType.REGION04, .Name = "中部"},
            New Region With {.Code = RegionType.REGION05, .Name = "近畿"},
            New Region With {.Code = RegionType.REGION06, .Name = "中国"},
            New Region With {.Code = RegionType.REGION07, .Name = "四国"},
            New Region With {.Code = RegionType.REGION08, .Name = "九州"},
            New Region With {.Code = RegionType.REGION09, .Name = "沖縄"}
        }

        ' 都道府県
        Me._prefectures = New ObservableCollection(Of Prefecture) From {
            New Prefecture With {.Code = "01", .Region = RegionType.REGION01, .Name = "北海道"},
            New Prefecture With {.Code = "02", .Region = RegionType.REGION02, .Name = "青森県"},
            New Prefecture With {.Code = "03", .Region = RegionType.REGION02, .Name = "岩手県"},
            New Prefecture With {.Code = "04", .Region = RegionType.REGION02, .Name = "宮城県"},
            New Prefecture With {.Code = "05", .Region = RegionType.REGION02, .Name = "秋田県"},
            New Prefecture With {.Code = "06", .Region = RegionType.REGION02, .Name = "山形県"},
            New Prefecture With {.Code = "07", .Region = RegionType.REGION02, .Name = "福島県"},
            New Prefecture With {.Code = "08", .Region = RegionType.REGION03, .Name = "茨城県"},
            New Prefecture With {.Code = "09", .Region = RegionType.REGION03, .Name = "栃木県"},
            New Prefecture With {.Code = "10", .Region = RegionType.REGION03, .Name = "群馬県"},
            New Prefecture With {.Code = "11", .Region = RegionType.REGION03, .Name = "埼玉県"},
            New Prefecture With {.Code = "12", .Region = RegionType.REGION03, .Name = "千葉県"},
            New Prefecture With {.Code = "13", .Region = RegionType.REGION03, .Name = "東京都"},
            New Prefecture With {.Code = "14", .Region = RegionType.REGION03, .Name = "神奈川県"},
            New Prefecture With {.Code = "15", .Region = RegionType.REGION04, .Name = "新潟県"},
            New Prefecture With {.Code = "16", .Region = RegionType.REGION04, .Name = "富山県"},
            New Prefecture With {.Code = "17", .Region = RegionType.REGION04, .Name = "石川県"},
            New Prefecture With {.Code = "18", .Region = RegionType.REGION04, .Name = "福井県"},
            New Prefecture With {.Code = "19", .Region = RegionType.REGION04, .Name = "山梨県"},
            New Prefecture With {.Code = "20", .Region = RegionType.REGION04, .Name = "長野県"},
            New Prefecture With {.Code = "21", .Region = RegionType.REGION04, .Name = "岐阜県"},
            New Prefecture With {.Code = "22", .Region = RegionType.REGION04, .Name = "静岡県"},
            New Prefecture With {.Code = "23", .Region = RegionType.REGION04, .Name = "愛知県"},
            New Prefecture With {.Code = "24", .Region = RegionType.REGION05, .Name = "三重県"},
            New Prefecture With {.Code = "25", .Region = RegionType.REGION05, .Name = "滋賀県"},
            New Prefecture With {.Code = "26", .Region = RegionType.REGION05, .Name = "京都府"},
            New Prefecture With {.Code = "27", .Region = RegionType.REGION05, .Name = "大阪府"},
            New Prefecture With {.Code = "28", .Region = RegionType.REGION05, .Name = "兵庫県"},
            New Prefecture With {.Code = "29", .Region = RegionType.REGION05, .Name = "奈良県"},
            New Prefecture With {.Code = "30", .Region = RegionType.REGION05, .Name = "和歌山県"},
            New Prefecture With {.Code = "31", .Region = RegionType.REGION06, .Name = "鳥取県"},
            New Prefecture With {.Code = "32", .Region = RegionType.REGION06, .Name = "島根県"},
            New Prefecture With {.Code = "33", .Region = RegionType.REGION06, .Name = "岡山県"},
            New Prefecture With {.Code = "34", .Region = RegionType.REGION06, .Name = "広島県"},
            New Prefecture With {.Code = "35", .Region = RegionType.REGION06, .Name = "山口県"},
            New Prefecture With {.Code = "36", .Region = RegionType.REGION07, .Name = "徳島県"},
            New Prefecture With {.Code = "37", .Region = RegionType.REGION07, .Name = "香川県"},
            New Prefecture With {.Code = "38", .Region = RegionType.REGION07, .Name = "愛媛県"},
            New Prefecture With {.Code = "39", .Region = RegionType.REGION07, .Name = "高知県"},
            New Prefecture With {.Code = "40", .Region = RegionType.REGION08, .Name = "福岡県"},
            New Prefecture With {.Code = "41", .Region = RegionType.REGION08, .Name = "佐賀県"},
            New Prefecture With {.Code = "42", .Region = RegionType.REGION08, .Name = "長崎県"},
            New Prefecture With {.Code = "43", .Region = RegionType.REGION08, .Name = "熊本県"},
            New Prefecture With {.Code = "44", .Region = RegionType.REGION08, .Name = "大分県"},
            New Prefecture With {.Code = "45", .Region = RegionType.REGION08, .Name = "宮崎県"},
            New Prefecture With {.Code = "46", .Region = RegionType.REGION08, .Name = "鹿児島県"},
            New Prefecture With {.Code = "47", .Region = RegionType.REGION09, .Name = "沖縄県"}
        }
    End Sub

End Class
